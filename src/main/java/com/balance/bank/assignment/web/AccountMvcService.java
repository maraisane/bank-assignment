package com.balance.bank.assignment.web;

import com.balance.bank.assignment.api.dto.CurrencyAccountsResponse;
import com.balance.bank.assignment.api.dto.TransactionalAccountsResponse;
import com.balance.bank.assignment.api.dto.WithdrawRequest;
import com.balance.bank.assignment.api.dto.WithdrawResponse;
import com.balance.bank.assignment.config.properties.DataApiConfigurationProperties;
import com.balance.bank.assignment.exception.QualifyingAccountNotFoundException;
import com.balance.bank.assignment.exception.WithdrawalFailedException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Date;

@Service
@Slf4j
public class AccountMvcService {
    private final RestTemplate restTemplate;
    private final DataApiConfigurationProperties properties;

    @Autowired
    public AccountMvcService(RestTemplate restTemplate, DataApiConfigurationProperties properties) {
        this.restTemplate = restTemplate;
        this.properties = properties;
    }

    public TransactionalAccountsResponse listTransactionalAccounts(int clientId, Date date) {
        final String url = properties.getBaseUrl()+"/api/transactional-accounts/" + clientId +"/"+ LocalDate.now();
        return restTemplate.exchange(url, HttpMethod.GET, new HttpEntity<>("parameters", createHeaders()), TransactionalAccountsResponse.class).getBody();
    }


    public CurrencyAccountsResponse listCurrencyAccountBalances(int clientId, Date date) {
        final String url = properties.getBaseUrl()+"/api/currency-accounts/" + clientId +"/"+LocalDate.now();
        return restTemplate.exchange(url, HttpMethod.GET, new HttpEntity<>("parameters", createHeaders()), CurrencyAccountsResponse.class).getBody();
    }


    public WithdrawResponse updateAccountBalances(int clientId, WithdrawRequest withdrawRequest) throws QualifyingAccountNotFoundException, WithdrawalFailedException {
        final String url = properties.getBaseUrl()+"/api/transactional-accounts/withdraw/" + clientId;
        WithdrawResponse withdrawResponse = null;

            ResponseEntity response = restTemplate.postForEntity(url, withdrawRequest,
                    WithdrawResponse.class);
            withdrawResponse= (WithdrawResponse)response.getBody();

        return withdrawResponse;
    }

    private HttpHeaders createHeaders() {
        return new HttpHeaders() {
            {
                setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
            }
        };
    }
}
