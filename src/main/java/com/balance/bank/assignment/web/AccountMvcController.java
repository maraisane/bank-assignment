package com.balance.bank.assignment.web;

import com.balance.bank.assignment.api.dto.CurrencyAccountsResponse;
import com.balance.bank.assignment.api.dto.TransactionalAccountsResponse;
import com.balance.bank.assignment.api.dto.WithdrawRequest;
import com.balance.bank.assignment.exception.QualifyingAccountNotFoundException;
import com.balance.bank.assignment.exception.WithdrawalFailedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Date;
import java.util.Optional;

@Controller
@RequestMapping("/")
public class AccountMvcController {

    @Autowired
    private AccountMvcService accountMvcService;


    @RequestMapping(path = {"/accounts/{id}"})
    public String listTransactionalAccounts(Model model, @PathVariable("id") Optional<Integer> id)
            throws QualifyingAccountNotFoundException
    {
        if (id.isPresent()) {
          TransactionalAccountsResponse accountsResponse =accountMvcService.listTransactionalAccounts(id.get(), new Date());
            model.addAttribute("accountsResponse", accountsResponse);
        } else {
            throw new QualifyingAccountNotFoundException();
        }
        return "list-transactional-accounts";
    }

    @RequestMapping(path = {"/currency/{id}"})
    public String listCurrencyAccountBalances(Model model, @PathVariable("id") Optional<Integer> id)
            throws QualifyingAccountNotFoundException
    {
        if (id.isPresent()) {
            CurrencyAccountsResponse currencyAccountsResponse =accountMvcService.listCurrencyAccountBalances(id.get(), new Date());
            model.addAttribute("currencyAccountsResponse", currencyAccountsResponse);
        } else {
            throw new QualifyingAccountNotFoundException();
        }
        return "list-currency-account-balances";
    }


    @RequestMapping(path = "/update/{id}",  method = RequestMethod.POST)
    public String updateAccount(Model model, @PathVariable("id") Optional<Integer> id, WithdrawRequest withdrawRequest)
            throws QualifyingAccountNotFoundException, WithdrawalFailedException {
         accountMvcService.updateAccountBalances(id.get(),withdrawRequest);
        return "dashboard";
    }

    @RequestMapping(path = {"/withdraw"})
    public String withdrawFromAccount(Model model) {
        model.addAttribute("withdrawRequest", new WithdrawRequest());

        return "draw-from-account";
    }

    @RequestMapping
    public String indexAll(Model model) {
        model.addAttribute("clientId", new Integer(10));
        return "dashboard";
    }
}
