package com.balance.bank.assignment.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.PRECONDITION_REQUIRED)
public class WithdrawalFailedException extends RuntimeException {

    public WithdrawalFailedException() {
        super();
    }

    public WithdrawalFailedException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public WithdrawalFailedException(final String message) {
        super(message);
    }

    public WithdrawalFailedException(final Throwable cause) {
        super(cause);
    }
}