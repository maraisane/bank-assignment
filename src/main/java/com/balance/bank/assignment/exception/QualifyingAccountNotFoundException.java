package com.balance.bank.assignment.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class QualifyingAccountNotFoundException extends Exception {

	private static final long serialVersionUID = 1L;
	public QualifyingAccountNotFoundException(){
		super();
	}
	public QualifyingAccountNotFoundException(String message) {
		super(message);
	}
	
	public QualifyingAccountNotFoundException(String message, Throwable t) {
		super(message, t);
	}
}
