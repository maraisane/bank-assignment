package com.balance.bank.assignment.config.properties;

import lombok.Data;
import javax.validation.constraints.NotEmpty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "routes.data")
@Data
public class DataApiConfigurationProperties {
    @NotEmpty
    private String baseUrl;
}
