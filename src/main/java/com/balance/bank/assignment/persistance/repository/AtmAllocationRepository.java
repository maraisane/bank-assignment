package com.balance.bank.assignment.persistance.repository;

import com.balance.bank.assignment.persistance.entity.Atm;
import com.balance.bank.assignment.persistance.entity.AtmAllocation;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AtmAllocationRepository extends CrudRepository<AtmAllocation, Integer> {

     Optional<List<AtmAllocation>> findAllAtmAllocationsByAtm(Atm atm);
}
