package com.balance.bank.assignment.persistance.repository;

import com.balance.bank.assignment.persistance.entity.ClientType;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientTypeRepository extends CrudRepository<ClientType, String> {

    <clientTypeCode> ClientType findByClientTypeCode(String accountTypeCode);
}
