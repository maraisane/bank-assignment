package com.balance.bank.assignment.persistance.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "ATM")
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Atm implements Serializable {

    @Id
    @Column(name = "ATM_ID")
    private Integer atmId;

    @Column(name = "NAME", length = 10, unique = true)
    private String name;

    @Column(name = "LOCATION", nullable = false, length = 255)
    private String location;
}
/*
CREATE TABLE ATM (
  ATM_ID   INTEGER IDENTITY PRIMARY KEY,
  NAME     VARCHAR(10)  NOT NULL UNIQUE,
  LOCATION VARCHAR(255) NOT NULL
);
 */
