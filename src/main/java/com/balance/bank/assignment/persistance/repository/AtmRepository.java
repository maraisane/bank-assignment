package com.balance.bank.assignment.persistance.repository;

import com.balance.bank.assignment.persistance.entity.Atm;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AtmRepository extends CrudRepository<Atm, Integer> {
}
