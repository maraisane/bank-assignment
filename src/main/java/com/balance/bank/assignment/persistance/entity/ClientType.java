package com.balance.bank.assignment.persistance.entity;

import lombok.*;

import javax.persistence.*;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Entity
@Table(name = "CLIENT_TYPE")
@EqualsAndHashCode(exclude = "clientSubTypes")
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ClientType {

    @Id
    @Column(name = "CLIENT_TYPE_CODE", nullable = false, length = 2)
    private String clientTypeCode;

    @Column(name = "DESCRIPTION", nullable = false, length = 255)
    private String description;

    @OneToMany(mappedBy = "clientType", cascade = CascadeType.ALL)
    private Set<ClientSubType> clientSubTypes;

    public ClientType(String clientTypeCode, ClientSubType... subTypes) {
        this.clientTypeCode = clientTypeCode;
        this.clientSubTypes = Stream.of(subTypes).collect(Collectors.toSet());
        this.clientSubTypes.forEach(x -> x.setClientType(this));
    }
}
