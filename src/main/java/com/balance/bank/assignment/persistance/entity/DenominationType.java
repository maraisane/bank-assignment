package com.balance.bank.assignment.persistance.entity;

import com.balance.bank.assignment.persistance.enums.DenominationTypeCode;
import lombok.*;

import javax.persistence.*;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Entity
@Table(name = "DENOMINATION_TYPE")
@EqualsAndHashCode(exclude = "denominations")
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class DenominationType {

    @Id
    @Column(name = "DENOMINATION_TYPE_CODE", nullable = false, length = 1)
    @Enumerated(EnumType.STRING)
    private DenominationTypeCode denominationTypeCode;

    @Column(name = "DESCRIPTION", nullable = false, length = 255)
    private String description;

    @OneToMany(mappedBy = "denominationType", cascade = CascadeType.ALL)
    private Set<Denomination> denominations;

    public DenominationType(DenominationTypeCode denominationTypeCode, Denomination... denominations) {
        this.denominationTypeCode = denominationTypeCode;
        this.denominations = Stream.of(denominations).collect(Collectors.toSet());
        this.denominations.forEach(x -> x.setDenominationType(this));
    }
}

/*
CREATE TABLE DENOMINATION_TYPE (
  DENOMINATION_TYPE_CODE VARCHAR(1)   NOT NULL PRIMARY KEY,
  DESCRIPTION            VARCHAR(255) NOT NULL,
);
 */
