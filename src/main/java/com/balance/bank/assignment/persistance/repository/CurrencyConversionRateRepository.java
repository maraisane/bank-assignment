package com.balance.bank.assignment.persistance.repository;

import com.balance.bank.assignment.persistance.entity.CurrencyConversionRate;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CurrencyConversionRateRepository extends CrudRepository<CurrencyConversionRate, String> {

    <currencyCode> Optional<CurrencyConversionRate> findCurrencyConversionRateByCurrencyCode(String currencyCode);
}
