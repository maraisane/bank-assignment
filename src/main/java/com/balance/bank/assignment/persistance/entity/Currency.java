package com.balance.bank.assignment.persistance.entity;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "CURRENCY")
@EqualsAndHashCode(exclude = "currencyConversionRate")
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Currency {

    @Id
    @Column(name = "CURRENCY_CODE", nullable = false, length = 3)
    private String currencyCode;

    @Column(name = "DECIMAL_PLACES", nullable = false)
    private Integer decimalPlaces;

    @Column(name = "DESCRIPTION", nullable = false, length = 255)
    private String description;

    @OneToOne(mappedBy = "currency", cascade = CascadeType.ALL)
    private CurrencyConversionRate currencyConversionRate;

    public Currency(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public Currency(String currencyCode, CurrencyConversionRate conversionRate) {
        this.currencyCode = currencyCode;
        this.currencyConversionRate = conversionRate;
    }
}
