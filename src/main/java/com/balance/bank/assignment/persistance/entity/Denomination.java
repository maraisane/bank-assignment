package com.balance.bank.assignment.persistance.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "DENOMINATION")
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Denomination {

    @Id
    @Column(name = "DENOMINATION_ID", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer denominationId;

    @Column(name = "VALUE", nullable = false)
    private BigDecimal value;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "DENOMINATION_TYPE_CODE")
    private DenominationType denominationType;
}

/*CREATE TABLE DENOMINATION (
        DENOMINATION_ID        INTEGER NOT NULL IDENTITY PRIMARY KEY,
        VALUE                  DECIMAL NOT NULL,
        DENOMINATION_TYPE_CODE VARCHAR(1) REFERENCES DENOMINATION_TYPE (DENOMINATION_TYPE_CODE)
        );
 */
