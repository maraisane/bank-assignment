package com.balance.bank.assignment.persistance.repository;

import com.balance.bank.assignment.persistance.entity.Currency;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CurrencyRepository extends CrudRepository<Currency, String> {

}
