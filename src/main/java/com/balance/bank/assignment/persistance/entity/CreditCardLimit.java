package com.balance.bank.assignment.persistance.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "CREDIT_CARD_LIMIT")
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class CreditCardLimit {

    @Id
    @Column(name = "CLIENT_ACCOUNT_NUMBER")
    private String accountNumber;

    @MapsId
    @OneToOne
    @JoinColumn(name = "CLIENT_ACCOUNT_NUMBER")
    private ClientAccount clientAccount;

    @Column(name = "ACCOUNT_LIMIT", nullable = false, precision = 18, scale = 8)
    private BigDecimal accountLimit;

    public CreditCardLimit(String accountNumber) {
        this.accountNumber = accountNumber;
    }
}
/*
 ****************************************************************************************
 *  CREDIT CARD LIMITS                                                                 *
 *  Lists the credit amount that a client is allowed to access on the specified        *
 *  Credit Card account                                                                *
 ***************************************************************************************
 */

   /* CREATE TABLE CREDIT_CARD_LIMIT (
        CLIENT_ACCOUNT_NUMBER VARCHAR(10)    NOT NULL PRIMARY KEY REFERENCES CLIENT_ACCOUNT (CLIENT_ACCOUNT_NUMBER),
        ACCOUNT_LIMIT         DECIMAL(18, 3) NOT NULL
        );*/
