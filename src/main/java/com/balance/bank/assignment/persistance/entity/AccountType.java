package com.balance.bank.assignment.persistance.entity;

import com.balance.bank.assignment.persistance.enums.AccountTypeCode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "ACCOUNT_TYPE")
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class AccountType implements Serializable {

    @Id
    @Column(name = "ACCOUNT_TYPE_CODE", nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    private AccountTypeCode accountTypeCode;

    @Column(name = "DESCRIPTION", nullable = false, length = 50)
    private String description;

    @Column(name = "TRANSACTIONAL", length = 1)
    private Boolean transactional;

    public AccountType(AccountTypeCode accountTypeCode) {
        this.accountTypeCode = accountTypeCode;
    }

    public AccountType(AccountTypeCode accountTypeCode, String description) {
        this.accountTypeCode = accountTypeCode;
        this.description = description;
    }

    public AccountType(AccountTypeCode accountTypeCode, String description, boolean transactional) {
        this.accountTypeCode = accountTypeCode;
        this.description = description;
        this.transactional = transactional;
    }
}
