package com.balance.bank.assignment.persistance.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "CURRENCY_CONVERSION_RATE")
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class CurrencyConversionRate {

    @Id
    @Column(name = "CURRENCY_CODE")
    private String currencyCode;

    @MapsId
    @OneToOne
    @JoinColumn(name = "CURRENCY_CODE", nullable = false)
    private Currency currency;

    @Column(name = "CONVERSION_INDICATOR", nullable = false, length = 1)
    private String conversionIndicator;

    @Column(name = "RATE", nullable = false, precision = 18, scale = 8)
    private BigDecimal rate;
}
