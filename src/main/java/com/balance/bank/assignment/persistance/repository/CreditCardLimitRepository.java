package com.balance.bank.assignment.persistance.repository;

import com.balance.bank.assignment.persistance.entity.ClientAccount;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CreditCardLimitRepository extends CrudRepository<ClientAccount, String> {
}
