package com.balance.bank.assignment.persistance.repository;

import com.balance.bank.assignment.persistance.entity.AccountType;
import com.balance.bank.assignment.persistance.entity.Client;
import com.balance.bank.assignment.persistance.entity.ClientAccount;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ClientAccountRepository extends CrudRepository<ClientAccount, String> {
    List<ClientAccount> findAllByClient(Client client);

    Iterable<ClientAccount> findAllByClientAndAccountType(Client client, AccountType accountType);

    Iterable<ClientAccount> findAllByClientAndAccountTypeIsIn(Client client, List<AccountType> accountTypes);

    Optional<ClientAccount> findClientAccountByClientAndAccountNumber(Client client, String accountNumber);
}
