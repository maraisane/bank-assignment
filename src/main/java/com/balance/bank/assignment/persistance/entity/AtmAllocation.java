package com.balance.bank.assignment.persistance.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Table(name = "ATM_ALLOCATION")
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class AtmAllocation {
    @Id
    @Column(name = "ATM_ALLOCATION_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer atmAllocationId;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "ATM_ID", nullable = false)
    private Atm atm;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "DENOMINATION_ID", nullable = false)
    private Denomination denomination;

    @Column(name = "COUNT", nullable = false)
    private Integer count;
}

