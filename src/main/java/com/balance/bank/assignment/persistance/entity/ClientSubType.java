package com.balance.bank.assignment.persistance.entity;

import lombok.*;

import javax.persistence.*;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Entity
@Data
@Table(name = "CLIENT_SUB_TYPE")
@EqualsAndHashCode(exclude = "clients")
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ClientSubType {
    @Id
    @Column(name = "CLIENT_SUB_TYPE_CODE", nullable = false, length = 4)
    private String clientSubTypeCode;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "CLIENT_TYPE_CODE")
    private ClientType clientType;

    @Column(name = "DESCRIPTION", nullable = false, length = 50)
    private String description;

    @OneToMany(mappedBy = "clientSubType", cascade = CascadeType.ALL)
    private Set<Client> clients;

    public ClientSubType(String clientSubTypeCode, Client... clients) {
        this.clientSubTypeCode = clientSubTypeCode;
        this.clients = Stream.of(clients).collect(Collectors.toSet());
        this.clients.forEach(x -> x.setClientSubType(this));
    }
}
