package com.balance.bank.assignment.persistance.repository;

import com.balance.bank.assignment.persistance.entity.Denomination;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DenominationRepository extends CrudRepository<Denomination, Integer> {

}
