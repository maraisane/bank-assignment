package com.balance.bank.assignment.persistance.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "CLIENT_ACCOUNT")
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ClientAccount {
    @Id
    @Column(name = "CLIENT_ACCOUNT_NUMBER", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String accountNumber;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "CLIENT_ID", nullable = false)
    private Client client;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "ACCOUNT_TYPE_CODE")
    private AccountType accountType;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "CURRENCY_CODE")
    private Currency currency;

    @Column(name = "DISPLAY_BALANCE", nullable = false, precision = 18, scale = 3)
    private BigDecimal displayBalance;

    @OneToOne(mappedBy = "clientAccount", cascade = CascadeType.ALL)
    private CreditCardLimit creditCardLimit;

    public ClientAccount(String accountNumber, CreditCardLimit creditCardLimit) {
        this.creditCardLimit = creditCardLimit;
        this.accountNumber = accountNumber;
    }
}


/***************************************************************************************
 *  CLIENT ACCOUNT BALANCES                                                            *
 *  SVGS and CFCA list positive account balances                                       *
 *  CHQ accounts allow being overdrawn and can have either a positive or               *
 *       negative balance                                                              *
 *  CCRD lists the current *available* balance on a credit card account, card limits   *
 *       are stored on the CREDIT_CARD_LIMIT table and lists the maximum credit amount *
 *       credit card accounts do now allow being overdrawn                             *
 *  PLOAN and HLOAN list outstanding balances on the loan amounts as negative amounts  *
 ***************************************************************************************/

  /*  CREATE TABLE CLIENT_ACCOUNT (
        CLIENT_ACCOUNT_NUMBER VARCHAR(10) IDENTITY PRIMARY KEY,
    CLIENT_ID             INTEGER     NOT NULL REFERENCES CLIENT (CLIENT_ID),
    ACCOUNT_TYPE_CODE     VARCHAR(10) NOT NULL REFERENCES ACCOUNT_TYPE (ACCOUNT_TYPE_CODE),
        CURRENCY_CODE         VARCHAR(3)  NOT NULL REFERENCES CURRENCY (CURRENCY_CODE),
        DISPLAY_BALANCE       NUMERIC(18, 3)
        );*/