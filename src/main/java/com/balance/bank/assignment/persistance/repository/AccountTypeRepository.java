package com.balance.bank.assignment.persistance.repository;

import com.balance.bank.assignment.persistance.entity.AccountType;
import com.balance.bank.assignment.persistance.enums.AccountTypeCode;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface AccountTypeRepository extends CrudRepository<AccountType, String> {

    <accountTypeCode> AccountType findByAccountTypeCode(AccountTypeCode accountTypeCode);

    Collection<AccountType> findAllByTransactionalIsTrue();

}
