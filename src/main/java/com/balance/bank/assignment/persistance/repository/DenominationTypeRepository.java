package com.balance.bank.assignment.persistance.repository;

import com.balance.bank.assignment.persistance.entity.DenominationType;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DenominationTypeRepository extends CrudRepository<DenominationType, String> {

}
