package com.balance.bank.assignment.persistance.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "CLIENT")
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Client {
    @Id
    @Column(name = "CLIENT_ID", nullable = false)
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer clientId;

    @Column(name = "TITLE", length = 10)
    private String tittle;

    @Column(name = "NAME", nullable = false, length = 255)
    private String name;

    @Column(name = "SURNAME", length = 100)
    private String surname;

    @Column(name = "DOB", nullable = false)
    private Date dob;

    @ManyToOne
    @JoinColumn(name = "CLIENT_SUB_TYPE_CODE")
    private ClientSubType clientSubType;

    public Client(Integer clientId) {
        this.clientId = clientId;
    }
}
/*
CREATE TABLE CLIENT (
  CLIENT_ID            INTEGER IDENTITY PRIMARY KEY,
  TITLE                VARCHAR(10),
  NAME                 VARCHAR(255) NOT NULL,
  SURNAME              VARCHAR(100),
  DOB                  DATE         NOT NULL,
  CLIENT_SUB_TYPE_CODE VARCHAR(4)   NOT NULL REFERENCES CLIENT_SUB_TYPE (CLIENT_SUB_TYPE_CODE)
);

 */
