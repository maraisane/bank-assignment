package com.balance.bank.assignment.persistance.repository;

import com.balance.bank.assignment.persistance.entity.ClientSubType;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientSubTypeRepository extends CrudRepository<ClientSubType, String> {

}
