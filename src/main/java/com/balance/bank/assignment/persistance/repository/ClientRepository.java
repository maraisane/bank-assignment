package com.balance.bank.assignment.persistance.repository;

import com.balance.bank.assignment.persistance.entity.Client;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientRepository extends CrudRepository<Client, Integer> {
}
