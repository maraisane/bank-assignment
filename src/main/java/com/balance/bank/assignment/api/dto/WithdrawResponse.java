package com.balance.bank.assignment.api.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.Map;


@Data
@NoArgsConstructor
public class WithdrawResponse {
    /*denomination, denominationCount*/
    @ApiModelProperty(name = "Denomination-Denomination-count")
    private Map<Integer, Integer> availableNotes = new HashMap<>();

    @ApiModelProperty(name = "Total amount withdrawn")
    private Double total;
}
