package com.balance.bank.assignment.api.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;



@Data
@NoArgsConstructor
public class TransactionalAccountsResponse {

    private List<TransactionalAccount> transactionalAccounts = new ArrayList<>();

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class TransactionalAccount {
        private String accountNumber;
        private String accountType;
        private BigDecimal accountBalance;
    }
}
