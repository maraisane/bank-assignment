package com.balance.bank.assignment.api.controller;

import com.balance.bank.assignment.api.dto.CurrencyAccountsResponse;
import com.balance.bank.assignment.api.dto.TransactionalAccountsResponse;
import com.balance.bank.assignment.api.dto.WithdrawRequest;
import com.balance.bank.assignment.api.dto.WithdrawResponse;
import com.balance.bank.assignment.exception.QualifyingAccountNotFoundException;
import com.balance.bank.assignment.exception.WithdrawalFailedException;
import com.balance.bank.assignment.service.AccountService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.stream.Collectors;

/**
 * Exposed the needed activities, as per the specification to any {@Code RestController}  consumer.
 *
 */

@RequiredArgsConstructor
@RestController
@RequestMapping(path = "/api")
@Validated
@Slf4j
public class AccountManagementResource {

    @Autowired
    private AccountService accountService;

    @GetMapping(path = "/transactional-accounts/{client-id}/{client-time-stamp}")
    public TransactionalAccountsResponse getTransactionalAccounts(@PathVariable("client-id") int clientId,
                                                                  @PathVariable("client-time-stamp")
                                                                  @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate clientTimeStamp)
            throws QualifyingAccountNotFoundException {

        TransactionalAccountsResponse response = accountService.getTransactionalAccounts(clientId, clientTimeStamp);
        response.setTransactionalAccounts(
                response.getTransactionalAccounts()
                        .stream()
                        .sorted(Comparator.comparing(TransactionalAccountsResponse.TransactionalAccount::getAccountBalance))
                        .collect(Collectors.toCollection(ArrayList::new)));
        return response;
    }

    @GetMapping(path = "/currency-accounts/{client-id}/{client-time-stamp}")
    public CurrencyAccountsResponse getCurrencyAccounts(@PathVariable("client-id") int clientId,
                                                        @PathVariable("client-time-stamp")
                                                        @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate clientTimeStamp)
            throws QualifyingAccountNotFoundException {
        log.info("Requesting transactional account display for client {} and from {}", clientId, clientTimeStamp);
        CurrencyAccountsResponse response = accountService
                .getCurrencyAccounts(clientId, clientTimeStamp);
        /*
         * Sorts the details and returns the response to the ATM terminal
         */
        response.setCurrencyAccounts(
                response.getCurrencyAccounts()
                        .stream()
                        .sorted(Comparator.comparing(CurrencyAccountsResponse.CurrencyAccount::getZarCurrencyBalance))
                        .collect(Collectors.toCollection(ArrayList::new)));

        return response;
    }

    @PostMapping(path = "/transactional-accounts/withdraw/{client-id}")
    @ResponseStatus(HttpStatus.OK)
    public WithdrawResponse withdrawCash(@PathVariable("client-id") Integer clientId, @RequestBody WithdrawRequest withdrawRequest)
            throws WithdrawalFailedException, QualifyingAccountNotFoundException {

        log.info("Requesting withdrawal account display for client {} and details {}", clientId, withdrawRequest);
        return accountService.updateClientAccount(clientId, withdrawRequest);
    }
}
