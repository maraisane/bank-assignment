package com.balance.bank.assignment.api.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


@Data
@NoArgsConstructor
public class CurrencyAccountsResponse {

    private List<CurrencyAccount> currencyAccounts = new ArrayList<>();

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class CurrencyAccount {

        private String accountNumber;
        private String currency;
        private BigDecimal currencyBalance;
        private String currencyConversionRate;
        private BigDecimal zarCurrencyBalance;
    }

}
