package com.balance.bank.assignment.service;

import com.balance.bank.assignment.api.dto.CurrencyAccountsResponse;
import com.balance.bank.assignment.api.dto.TransactionalAccountsResponse;
import com.balance.bank.assignment.api.dto.WithdrawRequest;
import com.balance.bank.assignment.api.dto.WithdrawResponse;
import com.balance.bank.assignment.atm.AtmLoadStatus;
import com.balance.bank.assignment.atm.AtmService;
import com.balance.bank.assignment.exception.QualifyingAccountNotFoundException;
import com.balance.bank.assignment.exception.WithdrawalFailedException;
import com.balance.bank.assignment.persistance.entity.Client;
import com.balance.bank.assignment.persistance.entity.ClientAccount;
import com.balance.bank.assignment.persistance.entity.CurrencyConversionRate;
import com.balance.bank.assignment.persistance.repository.ClientAccountRepository;
import com.balance.bank.assignment.persistance.repository.CurrencyConversionRateRepository;
import com.balance.bank.assignment.utils.BusinessRulesUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
public class AccountService {
    @Autowired
    private ClientAccountRepository clientAccountRepository;

    @Autowired
    private CurrencyConversionRateRepository currencyConversionRateRepository;

    @Autowired
    private AtmService atmService;

    public TransactionalAccountsResponse getTransactionalAccounts(int clientId, LocalDate accountDate) throws QualifyingAccountNotFoundException {
        List<ClientAccount> clientAccounts = getTransactionalClientsAccounts(clientId);

        TransactionalAccountsResponse response = new TransactionalAccountsResponse();

        List<ClientAccount> optionalClientAccounts =
                Optional.of(clientAccounts).orElseThrow(QualifyingAccountNotFoundException::new);

        if (optionalClientAccounts.isEmpty()) throw new QualifyingAccountNotFoundException();

        optionalClientAccounts
                .forEach((ClientAccount clientAccount) -> {
                    TransactionalAccountsResponse.TransactionalAccount transactionalAccount = new TransactionalAccountsResponse.TransactionalAccount();
                    transactionalAccount.setAccountType(clientAccount.getAccountType().getDescription());
                    transactionalAccount.setAccountBalance(clientAccount.getDisplayBalance().setScale(2, 2));
                    transactionalAccount.setAccountNumber(clientAccount.getAccountNumber());
                    response.getTransactionalAccounts().add(transactionalAccount);
                });

        return response;
    }

    public CurrencyAccountsResponse getCurrencyAccounts(int clientId, LocalDate accountDate) throws QualifyingAccountNotFoundException {
        CurrencyAccountsResponse response = new CurrencyAccountsResponse();

        List<ClientAccount> clientAccounts = getTransactionalClientsAccounts(clientId);

        if (clientAccounts.isEmpty())
            throw new QualifyingAccountNotFoundException();


        clientAccounts.forEach(account -> {
            CurrencyAccountsResponse.CurrencyAccount currencyAccount = new CurrencyAccountsResponse.CurrencyAccount();
            Optional<CurrencyConversionRate> currencyConversionRateOptional = currencyConversionRateRepository.
                    findCurrencyConversionRateByCurrencyCode(account.getCurrency().getCurrencyCode());
            String accountNumber = account.getAccountNumber();
            String currency = account.getCurrency().getCurrencyCode();
            BigDecimal currencyBalance = account.getDisplayBalance();
            BigDecimal currencyConversionRate = currencyConversionRateOptional.get().getRate();
            BigDecimal zarConvertedBalance = BusinessRulesUtils.getConvertedZarAmount(
                    currencyConversionRateOptional.get().getConversionIndicator(),
                    currencyBalance,
                    currencyConversionRate);
            currencyAccount.setAccountNumber(accountNumber);
            currencyAccount.setCurrency(currency);
            currencyAccount.setCurrencyBalance(currencyBalance.setScale(2, 2));
            currencyAccount.setCurrencyConversionRate(currencyConversionRate.toString());
            currencyAccount.setZarCurrencyBalance(zarConvertedBalance.setScale(2, 2));
            response.getCurrencyAccounts().add(currencyAccount);
        });
        return response;
    }


    public WithdrawResponse updateClientAccount(Integer clientId, WithdrawRequest withdrawRequest) throws WithdrawalFailedException, QualifyingAccountNotFoundException {
        WithdrawResponse response;
        Optional<ClientAccount> clientAccountOptional = clientAccountRepository
                .findClientAccountByClientAndAccountNumber(new Client(clientId), withdrawRequest.getAccountNumber());

        //  Validate client and account
        if (!clientAccountOptional.isPresent()) {
            throw new QualifyingAccountNotFoundException();
        }

        if (isAtmNotRegistered(withdrawRequest.getAtmId())) {
            throw new WithdrawalFailedException("ATM not registered or unfunded");
        }

        //        a.	Determine current balance  in ZAR
        BigDecimal accountBalance = getZarCurrentAccountBalance(clientAccountOptional);

        //        b.	Determine if the requested amount is available for withdrawal based on balance and limit rules
        if (!BusinessRulesUtils.isAmountAvailableToWithdraw(clientAccountOptional.get().getAccountType().getAccountTypeCode().name(),
                accountBalance, withdrawRequest.getAmount())) {
            throw new WithdrawalFailedException("Insufficient funds");
        }

        //        c.	Determine if the amount specified can be provided using only bank notes (no coins)
        int requestAmountInteger = withdrawRequest.getAmount().intValue();
        log.info(" User  Requested Amount in ZAR" + requestAmountInteger);
        Boolean dispatchOnlyNotes = (requestAmountInteger % 10) == 0;
        if (!dispatchOnlyNotes) {
            requestAmountInteger = requestAmountInteger - (requestAmountInteger % 10);
            log.warn(" Requested Amount Notes  -> " + requestAmountInteger);
        }

        //        d.	Determine the number of each denomination to dispense using the minimum number of notes using the available notes in the ATM
        AtmLoadStatus atmLoadStatus = atmService.calculateNoteWithdrawn(withdrawRequest.getAtmId(), requestAmountInteger);
        log.info("the number of each denomination to dispense using the minimum number of notes using the available notes in the ATM \n {\t{}", atmLoadStatus.getMapOutput());

        if (atmLoadStatus.getTotal() < requestAmountInteger || !dispatchOnlyNotes) {
            throw new WithdrawalFailedException("Amount not available, would you like to draw R" + atmLoadStatus.getTotal());
        }
        response = new WithdrawResponse();
        response.getAvailableNotes().putAll(atmLoadStatus.getMapOutput());
        response.setTotal(atmLoadStatus.getTotal());

        //        e.	The system adjusts the clients account balance with the amount requested
        ClientAccount toUpdateClientAccount = clientAccountOptional.get();
        toUpdateClientAccount.setDisplayBalance(toUpdateClientAccount.getDisplayBalance().subtract(new BigDecimal(atmLoadStatus.getTotal())));
        updateBalance(toUpdateClientAccount);

        return response;
    }


    private void updateBalance(ClientAccount clientAccount) {
        clientAccountRepository.save(clientAccount);
    }

    /*
     * gets the ZAR balance of the clients account and return the balance
     */
    private BigDecimal getZarCurrentAccountBalance(Optional<ClientAccount> clientAccountOptional) {

        String currencyCode = clientAccountOptional.get().getCurrency().getCurrencyCode();

        Optional<CurrencyConversionRate> currencyConversionRateOptional = currencyConversionRateRepository.
                findCurrencyConversionRateByCurrencyCode(currencyCode);

        BigDecimal currencyBalance = clientAccountOptional.get().getDisplayBalance();
        BigDecimal currencyConversionRate = currencyConversionRateOptional.get().getRate();

        return BusinessRulesUtils.getConvertedZarAmount(
                currencyConversionRateOptional.get().getConversionIndicator(),
                currencyBalance,
                currencyConversionRate);
    }


    private Boolean isAtmNotRegistered(Integer atmId) {
        return (!atmService.atmRegistered(atmId)
                || !atmService.atmAllocated(atmId));
        /*
         * Determine the number of each denomination to dispense using the minimum number of notes using the available notes in the ATM
         */
    }

    private List<ClientAccount> getTransactionalClientsAccounts(int clientId) {
        return clientAccountRepository
                .findAllByClient(new Client(clientId))
                .stream()
                .filter(clientAccount -> clientAccount.getAccountType().getTransactional())
                .collect(Collectors.toList());
    }
}
