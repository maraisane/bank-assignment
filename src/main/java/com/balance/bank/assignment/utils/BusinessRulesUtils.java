package com.balance.bank.assignment.utils;

import lombok.NonNull;

import java.math.BigDecimal;

public class BusinessRulesUtils {

    public static BigDecimal getConvertedZarAmount(@NonNull String conversionIndicator,
                                             @NonNull BigDecimal amount,
                                             @NonNull BigDecimal conversionRate) {
        if ( conversionIndicator.equals("/")) {
            return amount.divide(conversionRate).setScale(8);
        } else if ( conversionIndicator.equals("*")) {
            return amount.multiply(conversionRate).setScale(8);
        }
        return null;
    }

    public static  Boolean isAmountAvailableToWithdraw(@NonNull final String accountTypeCode,
                                                       @NonNull BigDecimal accountBalance,
                                                       @NonNull final BigDecimal requestedAmount) {
        BigDecimal amountForCheque = new BigDecimal(10000);
        if ("CHQ".equals(accountTypeCode)) {
            accountBalance = accountBalance.add(amountForCheque);
        }

        return requestedAmount.compareTo(accountBalance) <= 0;
    }
}
