package com.balance.bank.assignment.atm;

import lombok.extern.slf4j.Slf4j;

/**
 * The {@code DispenseProcessor} class implements all actions that takes place from the ATM. The
 * withdraw of any {@code com.balance.bank.assignment.persistance.entity.Denomination}, such as, say R200, are
 * implemented in here and the relevant subclass of {@code DispenseBrain} will just have to invoke this method.
 * <p>
 * The class is made final to support object immutability, and it can be shared easily:
 * <p>
 * The method, ProcessDispense, check the number of Nomination Notes allocated for the specific ATM. It then compares that
 * with the amount the client has requested. Then the correct number of Notes per Nomination will be dispensed, will all
 * relevant information  being kept in {@code AtmLoadStatus}
 *
 */
@Slf4j
public final class DispenseProcessor {

    /**
     * Process the query from the consumer of dispensing some notes from the ATM. It checks available notes, and counts
     * to fulfill the requested amount.
     *
     * @param currency              the amount to be dispensed
     * @param atmLoadStatus         the object that keeps the feedback info of how many notes have been dispensed
     * @param anyDispenseTillShelve The corresponding notes shelves inside the ATM. They might not be called that but it just made
     *                              my work easier by using that name.
     * @param <T>                   Any shelve that extends {@code DispenseBrain}
     */
    public <T extends DispenseBrain> void processDispense(Currency currency, AtmLoadStatus atmLoadStatus, T anyDispenseTillShelve) {
        int denominationCount = atmLoadStatus.getMapInput().get(anyDispenseTillShelve.getDenominationNote());
        int denominationAvailableAmount = denominationCount * anyDispenseTillShelve.getDenominationNote();
        int amountRequested = currency.getAmount();

        int remainder = 0;
        if (denominationAvailableAmount < amountRequested) {
            remainder = amountRequested - denominationAvailableAmount;
            amountRequested = denominationAvailableAmount;
        }

        int denominationCountAllocated = amountRequested / anyDispenseTillShelve.getDenominationNote();

        atmLoadStatus.getMapOutput().put(anyDispenseTillShelve.getDenominationNote(), denominationCountAllocated);

        if (amountRequested >= anyDispenseTillShelve.getDenominationNote()) {

            remainder = remainder + (amountRequested % anyDispenseTillShelve.getDenominationNote());

            log.info("Dispensing " + denominationCountAllocated + " R" + anyDispenseTillShelve.getDenominationNote() + " note");
            if (remainder != 0 && anyDispenseTillShelve.getDispenseBrain() != null) {
                anyDispenseTillShelve.getDispenseBrain().dispense(new Currency(remainder), anyDispenseTillShelve.getShelveAtmLoadStatus());
            }
        } else {
            anyDispenseTillShelve.getDispenseBrain().dispense(currency, anyDispenseTillShelve.getShelveAtmLoadStatus());
        }
    }
}
