package com.balance.bank.assignment.atm;

/**
 * {@link TillShelveDenominationBaseDispenser}
 *
 */
public class TillShelveDenomination200Dispenser extends TillShelveDenominationBaseDispenser {

    @Override
    public int getDenominationNote() {
        return 200;
    }

}
