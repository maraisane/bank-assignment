package com.balance.bank.assignment.atm;


/**
 * Acts as a contract on all the business functionality that must happen using the
 * ATM dispensing
 */
public interface DispenseBrain {

    /**
     * Given the amount to be dispensed, it loops through all the different Notes shelves
     *
     * @param dispenseBrain the actual implementer of the dispensing logic
     * @param atmLoadStatus the objects that holds all the data that will be from the DB, about how much to
     *                      dispense and how many notes have been allocated per registered ATM
     */
    void setNextDispenseShelve(DispenseBrain dispenseBrain, AtmLoadStatus atmLoadStatus);

    /**
     * Does computational of how many notes to dispense
     *
     * @param currency
     * @param atmLoadStatus
     */
    void dispense(Currency currency, AtmLoadStatus atmLoadStatus);

    /**
     * Holds the denomination note past in to be processed at the time
     *
     * @return
     */
    int getDenominationNote();

    /**
     * gives the hold feedback {@code AtmLoadStatus} when needed
     *
     * @return
     */
    AtmLoadStatus getShelveAtmLoadStatus();

    /**
     * Get the current {@code DispenseBrain}
     *
     * @return
     */
    DispenseBrain getDispenseBrain();

}
