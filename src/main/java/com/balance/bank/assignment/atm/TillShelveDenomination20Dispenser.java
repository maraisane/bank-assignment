package com.balance.bank.assignment.atm;

/**
 * {@link TillShelveDenominationBaseDispenser}
 */
public class TillShelveDenomination20Dispenser extends TillShelveDenominationBaseDispenser {
    @Override
    public int getDenominationNote() {
        return 20;
    }
}
