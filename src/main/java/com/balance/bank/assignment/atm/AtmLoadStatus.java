package com.balance.bank.assignment.atm;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * A serialised object that holds data to and from the ATM {@code DispenseProcessor}
 */
@Data
public class AtmLoadStatus implements Serializable {

    @Setter(AccessLevel.NONE)
    @Getter(AccessLevel.NONE)
    private Double total;

    private Double amountRequest;

    private Map<Integer, Integer> mapInput = new HashMap();

    private Map<Integer, Integer> mapOutput = new HashMap();

    public Double getTotal() {
        Double totalCalculate = 0.0;
        for (Map.Entry<Integer, Integer> entry : mapOutput.entrySet()) {
            totalCalculate += (entry.getKey() * entry.getValue());
        }
        return totalCalculate;
    }

    @Override
    public String toString() {
        return "AtmLoadStatus\n{\n" + "total=" + getTotal() + ",\n amountRequest="
                + amountRequest + ",\n mapInput=" + mapInput + ",\n mapOutput=" + mapOutput + "\n}";
    }
}
