package com.balance.bank.assignment.atm;

import com.balance.bank.assignment.persistance.entity.Atm;
import com.balance.bank.assignment.persistance.entity.AtmAllocation;
import com.balance.bank.assignment.persistance.entity.Denomination;
import com.balance.bank.assignment.persistance.repository.AtmAllocationRepository;
import com.balance.bank.assignment.persistance.repository.AtmRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Exposes the functionality of the ATM to the other class that may need it. For Example,
 * {@code TransactionAccountService}
 */

@Service
@Slf4j
@RequiredArgsConstructor
public class AtmService {

    @Autowired
    private AtmRepository atmRepository;

    @Autowired
    private AtmAllocationRepository atmAllocationRepository;

    public Boolean atmRegistered(Integer atmId) {
        return atmRepository.findById(atmId)
                .filter(atm -> atm.getAtmId().equals(atmId)).isPresent();
    }

    public Boolean atmAllocated(Integer atmId) {
        return getAtmAllocations(atmId).isPresent();
    }

    public Optional<List<AtmAllocation>> getProposedAmount(Integer atmId, BigDecimal requestedAmount) {
        return getAtmAllocations(atmId);
    }

    public Optional<List<AtmAllocation>> getGetAtmAllocations(Integer atmId) {
        return getAtmAllocations(atmId);
    }

    private Optional<List<AtmAllocation>> getAtmAllocations(Integer atmId) {
        Atm atm = new Atm();
        atm.setAtmId(atmId);
        return atmAllocationRepository.findAllAtmAllocationsByAtm(atm);
    }

    public AtmLoadStatus calculateNoteWithdrawn(Integer atmId, Integer requestAmount) {
        Map<Integer, Integer> notesAllocateMap = new HashMap<>();

        for (AtmAllocation atmAllocation : getAtmAllocations(atmId).get()) {
            final Denomination denomination = atmAllocation.getDenomination();

            if (denomination.getDenominationType().equals("C")) {
                continue;
            }
            Integer denominationNote = denomination.getValue().intValue();
            Integer denominationCount = atmAllocation.getCount();
            notesAllocateMap.put(denominationNote, denominationCount);
        }

        return getAtmLoadProcess(notesAllocateMap, requestAmount);
    }

    public AtmLoadStatus getAtmLoadProcess(Map<Integer, Integer> map, Integer requestAmount) {
        DispenseBrain firstDispenseBrain;
        // initialize the chain
        firstDispenseBrain = new TillShelveDenomination200Dispenser();
        DispenseBrain dispenseBrain2 = new TillShelveDenomination100Dispenser();
        DispenseBrain dispenseBrain3 = new TillShelveDenomination50Dispenser();
        DispenseBrain dispenseBrain4 = new TillShelveDenomination20Dispenser();
        DispenseBrain dispenseBrain5 = new TillShelveDenomination10Dispenser();

        AtmLoadStatus atmLoadStatus = new AtmLoadStatus();
        atmLoadStatus.getMapInput().putAll(map);
        atmLoadStatus.setAmountRequest(Double.valueOf(requestAmount));

        // set the chain of responsibility
        firstDispenseBrain.setNextDispenseShelve(dispenseBrain2, atmLoadStatus);
        dispenseBrain2.setNextDispenseShelve(dispenseBrain3, atmLoadStatus);
        dispenseBrain3.setNextDispenseShelve(dispenseBrain4, atmLoadStatus);
        dispenseBrain4.setNextDispenseShelve(dispenseBrain5, atmLoadStatus);

        if (requestAmount % 10 != 0) {
            log.warn("Amount should be in multiple of R10.  " +requestAmount );
            requestAmount = requestAmount - (requestAmount % 10);

            log.warn("Amount should be in multiple of R10.  [ NEW] " +requestAmount );
        }
        // process the request
        firstDispenseBrain.dispense(new Currency(requestAmount), atmLoadStatus);
        log.info(dispenseBrain4.getShelveAtmLoadStatus().toString());
        return dispenseBrain4.getShelveAtmLoadStatus();
    }
}
