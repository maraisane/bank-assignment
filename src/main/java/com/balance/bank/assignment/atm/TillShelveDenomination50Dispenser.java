package com.balance.bank.assignment.atm;

/**
 * {@link TillShelveDenominationBaseDispenser}
 *
 */
public class TillShelveDenomination50Dispenser extends TillShelveDenominationBaseDispenser {

    @Override
    public int getDenominationNote() {
        return 50;
    }

}
