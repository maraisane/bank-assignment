package com.balance.bank.assignment.atm;


/**
 * The base class that abstracted all the common function for any class that is
 * a subclass of {@code DispenseBrain}
 *
 */
public abstract class TillShelveDenominationBaseDispenser implements DispenseBrain {
    protected DispenseBrain dispenseBrain;
    protected AtmLoadStatus shelveAtmLoadStatus;
    protected DispenseProcessor dispenseProcessor;

    @Override
    public void setNextDispenseShelve(DispenseBrain dispenseBrain, AtmLoadStatus shelveAtmLoadStatus) {
        this.dispenseBrain = dispenseBrain;
        this.shelveAtmLoadStatus = shelveAtmLoadStatus;
    }

    @Override
    public void dispense(Currency currency, AtmLoadStatus atmLoadStatus) {
        dispenseProcessor = new DispenseProcessor();
        dispenseProcessor.processDispense(currency, atmLoadStatus, this);
    }

    @Override
    public AtmLoadStatus getShelveAtmLoadStatus() {
        return shelveAtmLoadStatus;
    }

    @Override
    public DispenseBrain getDispenseBrain() {
        return dispenseBrain;
    }

}
