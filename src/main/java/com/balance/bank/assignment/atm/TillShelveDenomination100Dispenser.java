package com.balance.bank.assignment.atm;

/**
 * {@link TillShelveDenominationBaseDispenser}
 *
 */
public class TillShelveDenomination100Dispenser extends TillShelveDenominationBaseDispenser {

    @Override
    public int getDenominationNote() {
        return 100;
    }

}
