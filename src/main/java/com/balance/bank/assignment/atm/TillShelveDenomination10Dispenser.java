package com.balance.bank.assignment.atm;


/**
 * {@link TillShelveDenominationBaseDispenser}
 *
 */
public class TillShelveDenomination10Dispenser extends TillShelveDenominationBaseDispenser {

    @Override
    public int getDenominationNote() {
        return 10;
    }
}
