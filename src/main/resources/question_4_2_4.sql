-------------------------------------------------------------------------------------------
-- 4.2.4  Reporting – Find the transactional account per client with the highest balance --
-------------------------------------------------------------------------------------------

SELECT TEMP2.CLIENT_ID, OUTER1.SURNAME, OUTER1.CLIENT_ACCOUNT_NUMBER,OUTER1.DESCRIPTION, TEMP2.HIGHEST
FROM (
       SELECT TEMP.CLIENT_ID, MAX(TEMP.DISPLAY_BALANCE) HIGHEST
       FROM (
              SELECT AT.TRANSACTIONAL,
                     CA.CLIENT_ID,
                     C.SURNAME,
                     CA.CLIENT_ACCOUNT_NUMBER,
                     AT.DESCRIPTION,
                     CA.DISPLAY_BALANCE
              FROM CLIENT_ACCOUNT CA
                     INNER JOIN CLIENT C ON C.CLIENT_ID = CA.CLIENT_ID
                     INNER JOIN ACCOUNT_TYPE AT ON AT.ACCOUNT_TYPE_CODE = CA.ACCOUNT_TYPE_CODE
              WHERE AT.TRANSACTIONAL = TRUE
              GROUP BY CA.CLIENT_ID, CA.CLIENT_ACCOUNT_NUMBER) AS TEMP
       GROUP BY TEMP.CLIENT_ID) AS TEMP2
       INNER JOIN (SELECT AT.TRANSACTIONAL,
                          CA.CLIENT_ID,
                          C.SURNAME,
                          CA.CLIENT_ACCOUNT_NUMBER,
                          AT.DESCRIPTION,
                          CA.DISPLAY_BALANCE
                   FROM CLIENT_ACCOUNT CA
                          INNER JOIN CLIENT C ON C.CLIENT_ID = CA.CLIENT_ID
                          INNER JOIN ACCOUNT_TYPE AT ON AT.ACCOUNT_TYPE_CODE = CA.ACCOUNT_TYPE_CODE
                   WHERE AT.TRANSACTIONAL = TRUE
                   GROUP BY CA.CLIENT_ID, CA.CLIENT_ACCOUNT_NUMBER) AS OUTER1
                  ON OUTER1.CLIENT_ID = TEMP2.CLIENT_ID AND OUTER1.DISPLAY_BALANCE = TEMP2.HIGHEST;
