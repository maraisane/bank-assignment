-------------------------------------------------------------------------------------------
-- 4.2.4  Reporting – Find the transactional account per client with the highest balance --
-------------------------------------------------------------------------------------------

SELECT TEMP2.CLIENT_ID, OUTER1.SURNAME, OUTER1.CLIENT_ACCOUNT_NUMBER,OUTER1.DESCRIPTION, TEMP2.HIGHEST
FROM (
       SELECT TEMP.CLIENT_ID, MAX(TEMP.DISPLAY_BALANCE) HIGHEST
       FROM (
              SELECT AT.TRANSACTIONAL,
                     CA.CLIENT_ID,
                     C.SURNAME,
                     CA.CLIENT_ACCOUNT_NUMBER,
                     AT.DESCRIPTION,
                     CA.DISPLAY_BALANCE
              FROM CLIENT_ACCOUNT CA
                     INNER JOIN CLIENT C ON C.CLIENT_ID = CA.CLIENT_ID
                     INNER JOIN ACCOUNT_TYPE AT ON AT.ACCOUNT_TYPE_CODE = CA.ACCOUNT_TYPE_CODE
              WHERE AT.TRANSACTIONAL = TRUE
              GROUP BY CA.CLIENT_ID, CA.CLIENT_ACCOUNT_NUMBER) AS TEMP
       GROUP BY TEMP.CLIENT_ID) AS TEMP2
       INNER JOIN (SELECT AT.TRANSACTIONAL,
                          CA.CLIENT_ID,
                          C.SURNAME,
                          CA.CLIENT_ACCOUNT_NUMBER,
                          AT.DESCRIPTION,
                          CA.DISPLAY_BALANCE
                   FROM CLIENT_ACCOUNT CA
                          INNER JOIN CLIENT C ON C.CLIENT_ID = CA.CLIENT_ID
                          INNER JOIN ACCOUNT_TYPE AT ON AT.ACCOUNT_TYPE_CODE = CA.ACCOUNT_TYPE_CODE
                   WHERE AT.TRANSACTIONAL = TRUE
                   GROUP BY CA.CLIENT_ID, CA.CLIENT_ACCOUNT_NUMBER) AS OUTER1
                  ON OUTER1.CLIENT_ID = TEMP2.CLIENT_ID AND OUTER1.DISPLAY_BALANCE = TEMP2.HIGHEST;

-------------------------------------------------------------------------------------------
-- 4.2.5  Reporting – Calculate aggregate financial position per client --
-------------------------------------------------------------------------------------------
------------------------------------------------------
--    Step 1.
------------------------------------------------------
CREATE TEMP TABLE DONE_NAME_T AS
SELECT *
FROM (
       --NAMES
       SELECT CLIENT_ID, TITLE || ' ' || NAME || ' ' || SURNAME CLIENT_NAME
       FROM (
              SELECT AT.TRANSACTIONAL,
                     CA.CLIENT_ID,
                     C.TITLE,
                     C.NAME,
                     C.SURNAME,
                     CA.CLIENT_ACCOUNT_NUMBER,
                     AT.DESCRIPTION,
                     AT.ACCOUNT_TYPE_CODE,
                     CA.DISPLAY_BALANCE
              FROM CLIENT_ACCOUNT CA
                     INNER JOIN CLIENT C ON C.CLIENT_ID = CA.CLIENT_ID
                     INNER JOIN ACCOUNT_TYPE AT ON AT.ACCOUNT_TYPE_CODE = CA.ACCOUNT_TYPE_CODE
            ) AS NAME
       GROUP BY CLIENT_ID) AS DONE_NAME;

------------------------------------------------------
--    Step 2.
------------------------------------------------------
CREATE TEMP TABLE DONE_LOAN_T AS
SELECT *
FROM (
       --LOAN BALANCE FOR CLIENT
       SELECT CLIENT_ID, SUM(DISPLAY_BALANCE) LOAN_BAL
       FROM (
              SELECT AT.TRANSACTIONAL,
                     CA.CLIENT_ID,
                     C.SURNAME,
                     CA.CLIENT_ACCOUNT_NUMBER,
                     AT.DESCRIPTION,
                     AT.ACCOUNT_TYPE_CODE,
                     CA.DISPLAY_BALANCE
              FROM CLIENT_ACCOUNT CA
                     INNER JOIN CLIENT C ON C.CLIENT_ID = CA.CLIENT_ID
                     INNER JOIN ACCOUNT_TYPE AT ON AT.ACCOUNT_TYPE_CODE = CA.ACCOUNT_TYPE_CODE
              WHERE AT.ACCOUNT_TYPE_CODE IN ('PLOAN', 'HLOAN')
            ) AS LOAN
       GROUP BY CLIENT_ID) AS DONE_LOAN;

------------------------------------------------------
--   Step 3.
------------------------------------------------------
CREATE TEMP TABLE DONE_TRANS_T AS
SELECT *
FROM (
       -- T BALANCE FOR CLIENT
       SELECT CLIENT_ID, SUM(DISPLAY_BALANCE) TRANS_BAL
       FROM (
              SELECT AT.TRANSACTIONAL,
                     CA.CLIENT_ID,
                     C.SURNAME,
                     CA.CLIENT_ACCOUNT_NUMBER,
                     AT.DESCRIPTION,
                     AT.ACCOUNT_TYPE_CODE,
                     CA.DISPLAY_BALANCE
              FROM CLIENT_ACCOUNT CA
                     INNER JOIN CLIENT C ON C.CLIENT_ID = CA.CLIENT_ID
                     INNER JOIN ACCOUNT_TYPE AT ON AT.ACCOUNT_TYPE_CODE = CA.ACCOUNT_TYPE_CODE
              WHERE AT.TRANSACTIONAL = TRUE
            ) AS TRA
       GROUP BY CLIENT_ID) AS DONE_TRANS;

------------------------------------------------------
--    Step 4.
------------------------------------------------------
CREATE TEMP TABLE DONE_ALL_T AS
SELECT *
FROM (
       --ALL ACCOUNTS
       SELECT CLIENT_ID, SUM(DISPLAY_BALANCE) ALL_BAL
       FROM (
              SELECT AT.TRANSACTIONAL,
                     CA.CLIENT_ID,
                     C.SURNAME,
                     CA.CLIENT_ACCOUNT_NUMBER,
                     AT.DESCRIPTION,
                     AT.ACCOUNT_TYPE_CODE,
                     CA.DISPLAY_BALANCE
              FROM CLIENT_ACCOUNT CA
                     INNER JOIN CLIENT C ON C.CLIENT_ID = CA.CLIENT_ID
                     INNER JOIN ACCOUNT_TYPE AT ON AT.ACCOUNT_TYPE_CODE = CA.ACCOUNT_TYPE_CODE
            ) AS ALL
       GROUP BY CLIENT_ID) AS DONE_ALL;

------------------------------------------------------
--    Final results .
------------------------------------------------------
SELECT DONE_NAME_T.CLIENT_NAME "Client", DONE_LOAN_T.LOAN_BAL "Loan Balance", DONE_TRANS_T.TRANS_BAL "Transactional Balance", DONE_ALL_T.ALL_BAL "Net Position"
FROM DONE_NAME_T
       LEFT JOIN DONE_ALL_T ON DONE_ALL_T.CLIENT_ID = DONE_NAME_T.CLIENT_ID
       LEFT JOIN DONE_LOAN_T ON DONE_LOAN_T.CLIENT_ID = DONE_NAME_T.CLIENT_ID
       LEFT JOIN DONE_TRANS_T ON DONE_TRANS_T.CLIENT_ID = DONE_NAME_T.CLIENT_ID;