package com.balance.bank.assignment.atm;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class AtmServiceTest {
    @InjectMocks
    private AtmService atmService;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void givenDenominationNoteAndItsCountOnAtmAndRequestAmount_whenResults_thenResults() {
        //Given
        AtmLoadStatus atmLoadStatus = new AtmLoadStatus();
        Map<Integer, Integer> map = new HashMap<>();
        //denomination, count
        map.put(200, 2);
        map.put(100, 3);
        map.put(50, 4);
        map.put(20, 5);
        map.put(10, 50);

        atmLoadStatus.getMapInput().putAll(map);

        //when
        AtmLoadStatus atmLoadStatusExpected = atmService.getAtmLoadProcess(map, 1400);
        System.out.println(atmLoadStatusExpected.toString());

        assertThat(atmLoadStatusExpected.toString())
                .isSubstringOf("{\n" +
                        "total=1400.0,\n" +
                        " amountRequest=1400.0,\n" +
                        " mapInput={200=2, 50=4, 10=50, 100=3, 20=5},\n" +
                        " mapOutput={50=4, 100=3, 20=5, 200=2, 10=40}\n" +
                        "}\n" +
                        "AtmLoadStatus\n" +
                        "{\n" +
                        "total=1400.0,\n" +
                        " amountRequest=1400.0,\n" +
                        " mapInput={200=2, 50=4, 10=50, 100=3, 20=5},\n" +
                        " mapOutput={50=4, 100=3, 20=5, 200=2, 10=40}\n" +
                        "}");
    }

}