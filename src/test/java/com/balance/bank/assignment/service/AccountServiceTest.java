package com.balance.bank.assignment.service;

import com.balance.bank.assignment.api.dto.CurrencyAccountsResponse;
import com.balance.bank.assignment.api.dto.TransactionalAccountsResponse;
import com.balance.bank.assignment.api.dto.WithdrawRequest;
import com.balance.bank.assignment.atm.AtmService;
import com.balance.bank.assignment.exception.QualifyingAccountNotFoundException;
import com.balance.bank.assignment.exception.WithdrawalFailedException;
import com.balance.bank.assignment.persistance.entity.*;
import com.balance.bank.assignment.persistance.enums.AccountTypeCode;
import com.balance.bank.assignment.persistance.repository.AccountTypeRepository;
import com.balance.bank.assignment.persistance.repository.ClientAccountRepository;
import com.balance.bank.assignment.persistance.repository.CurrencyConversionRateRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.util.Assert;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;


public class AccountServiceTest {

    @InjectMocks
    private AccountService accountService;

    @Mock
    private AtmService atmService;

    @Mock
    private ClientAccountRepository clientAccountRepository;

    private ClientAccount clientAccount;

    @Mock
    private AccountTypeRepository accountTypeRepository;

    @Mock
    private CurrencyConversionRateRepository currencyConversionRateRepository;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);

        clientAccount = new ClientAccount(
                "4067342946",
                new Client(1),
                new AccountType(AccountTypeCode.CHQ, "Cheque Account"),
                null,
                new BigDecimal(200),
                new CreditCardLimit("4055230225"));


    }


    @Test
    public void givenClient_whenGetTransactionalAccounts_thenReturnAccounts() throws QualifyingAccountNotFoundException {
        //given
        int clientId = 1;
        final LocalDate clientDate =LocalDate.now().minusWeeks(1);

        //FIXME use functional way
        List<ClientAccount> clientAccounts = Arrays.asList(
                new ClientAccount("4067342946", new Client(1),
                        new AccountType(AccountTypeCode.CHQ, "Cheque Account", true),
                        new Currency("ZAR"),
                        new BigDecimal(200),
                        new CreditCardLimit("4055230225")),
                new ClientAccount(
                        "4055230226",
                        new Client(1),
                        new AccountType(AccountTypeCode.SVGS, "Saving Account", false),
                        new Currency("ZAR"),
                        new BigDecimal(200),
                        new CreditCardLimit("4055230226")));

        when(clientAccountRepository.findAllByClient( new Client(1))).thenReturn(clientAccounts);


        TransactionalAccountsResponse transactionalAccountsResponse = accountService.getTransactionalAccounts(clientId,
                clientDate);


        assertThat(transactionalAccountsResponse.getTransactionalAccounts().size())
                .isEqualTo(1);

    }

    @Test(expected = QualifyingAccountNotFoundException.class)
    public void givenClient_whenGetTransactionalAccounts_thenReturnError() throws QualifyingAccountNotFoundException {

        final Client client = new Client(1);
        final LocalDate clientDate =LocalDate.now().minusWeeks(1);

        List<ClientAccount> clientAccounts = Arrays.asList(
                new ClientAccount("4067342946", client,
                        new AccountType(AccountTypeCode.CHQ, "Cheque Account", false),
                        new Currency("ZAR"),
                        new BigDecimal(200),
                        new CreditCardLimit("4055230225")),
                new ClientAccount(
                        "4055230226",
                        new Client(1),
                        new AccountType(AccountTypeCode.SVGS, "Saving Account", false),
                        new Currency("ZAR"),
                        new BigDecimal(200),
                        new CreditCardLimit("4055230226")));



        when(clientAccountRepository.findAllByClient( new Client(1))).thenReturn(clientAccounts);
        TransactionalAccountsResponse transactionalAccountsResponse = accountService.getTransactionalAccounts(1,
                clientDate);

    }

    @Test
    public void givenClient_whenGetCurrencyAccounts_thenReturnAccounts() throws QualifyingAccountNotFoundException {
        //given
        int clientId = 1;
        final LocalDate clientDate =LocalDate.now().minusWeeks(1);

        //FIXME use functional way
        List<ClientAccount> clientAccounts = Arrays.asList(
                new ClientAccount("4067342946", new Client(1),
                        new AccountType(AccountTypeCode.CHQ, "Cheque Account", true),
                        new Currency("ZAR"),
                        new BigDecimal(200),
                        new CreditCardLimit("4055230225")),
                new ClientAccount(
                        "4055230226",
                        new Client(1),
                        new AccountType(AccountTypeCode.SVGS, "Saving Account", false),
                        new Currency("ZAR"),
                        new BigDecimal(200),
                        new CreditCardLimit("4055230226")));

        when(clientAccountRepository.findAllByClient( new Client(1))).thenReturn(clientAccounts);

        CurrencyConversionRate currencyConversionRate =
                new CurrencyConversionRate("USD",
                        new Currency("USD"),
                        "*", new BigDecimal("14.29"));
        Optional<CurrencyConversionRate> currencyConversionRateOptional = Optional.of(currencyConversionRate);
        when(currencyConversionRateRepository.
                findCurrencyConversionRateByCurrencyCode(any())).thenReturn(currencyConversionRateOptional);

        //when
        CurrencyAccountsResponse currencyAccountsResponse = accountService.getCurrencyAccounts(clientId,
                clientDate);

        // then
        assertThat(currencyAccountsResponse.getCurrencyAccounts().size())
                .isEqualTo(1);

        InOrder inOrder = inOrder( clientAccountRepository, currencyConversionRateRepository);
        //then

    }


    @Test(expected = QualifyingAccountNotFoundException.class)
    public void givenClientAndAccountAndAmount_whenUpdateAccount_throwsQualifyingAccountNotFoundException()  throws QualifyingAccountNotFoundException {
        when(clientAccountRepository.
                findClientAccountByClientAndAccountNumber(any(),any()))
                .thenReturn(Optional
                        .ofNullable(null));

        WithdrawRequest request = new WithdrawRequest();
        request.setAccountNumber("4055230225");
        request.setAtmId(1);
        request.setAmount(BigDecimal.TEN);
        request.setClientTimeStamp(LocalDate.now());

        accountService.updateClientAccount(1,request);
    }

    @Test(expected = WithdrawalFailedException.class)
    public void givenInvalidAtm_whenUpdateAccount_throwsWithdrawalFailedException() throws WithdrawalFailedException, QualifyingAccountNotFoundException {
        when(clientAccountRepository.
                findClientAccountByClientAndAccountNumber(any(),any()))
                .thenReturn(Optional
                        .ofNullable(clientAccount));

        when(atmService.atmAllocated(anyInt())).thenReturn(false);

        WithdrawRequest request = new WithdrawRequest();
        request.setAccountNumber("4055230225");
        request.setAtmId(1);
        request.setAmount(BigDecimal.TEN);
        request.setClientTimeStamp(LocalDate.now());

        accountService.updateClientAccount(1,request);
    }
}