package com.balance.bank.assignment.api.controller;

import com.balance.bank.assignment.api.dto.TransactionalAccountsResponse;
import com.balance.bank.assignment.service.AccountService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.time.LocalDate;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest(AccountManagementResource.class)
public class AccountManagementResourceTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private AccountService accountService;

    @Test
    public void givenClientIdAndAccountDate_whenGetTransactionalAccounts_thenReturnJsonArrayAccounts()
            throws Exception {
        //given
        final String clientId = "10";
        final LocalDate accountDate =LocalDate.now().minusWeeks(1);
        final String uri = "/api/transactional-accounts/" + clientId + "/" + accountDate;
        final TransactionalAccountsResponse response = new TransactionalAccountsResponse();
        response.getTransactionalAccounts().add(
                new TransactionalAccountsResponse.TransactionalAccount(
                        "10",
                        "CRD",
                        BigDecimal.TEN));
        given(accountService.getTransactionalAccounts(Integer.valueOf(clientId), accountDate)).willReturn(response);

        //then
        mockMvc.perform(get(uri)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(header().doesNotExist("password"))
                .andExpect(content().json("{\"transactionalAccounts\":[{\"accountNumber\":\"10\"," +
                        "\"accountType\":\"CRD\",\"accountBalance\":10}]}        "));
    }
}