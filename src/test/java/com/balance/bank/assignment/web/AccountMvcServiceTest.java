package com.balance.bank.assignment.web;

import com.balance.bank.assignment.api.dto.TransactionalAccountsResponse;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@ActiveProfiles({"default", "local"})
public class AccountMvcServiceTest {

    @Autowired
    private AccountMvcService accountMvcService;


    @Test
    public void given_clientIdAndDate_listTransactionAccounts_thenReturnAllAccounts() {
        TransactionalAccountsResponse transactionalAccounts = accountMvcService.listTransactionalAccounts(1, new Date());

        Assert.assertNotNull("The actual String  should not be null",transactionalAccounts );
    }

}