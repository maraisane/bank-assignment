package com.balance.bank.assignment.persistance.repository;

import com.balance.bank.assignment.persistance.entity.AccountType;
import com.balance.bank.assignment.persistance.entity.Client;
import com.balance.bank.assignment.persistance.entity.ClientAccount;
import com.balance.bank.assignment.persistance.enums.AccountTypeCode;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ClientAccountRepositoryTest {
    @Autowired
    private ClientAccountRepository clientAccountRepository;

    @Test
    public void whenCountAll_thenReturnTotal() {
        assertThat(clientAccountRepository.count())
                .isGreaterThan(0);
    }

    @Test
    public void whenFindAllByClientAndAccountType_thenReturnCorrectClientAccounts() {
        // Given
        Client client = new Client();
        client.setClientId(10);
        // And
        AccountType accountType = new AccountType();
        accountType.setAccountTypeCode(AccountTypeCode.CHQ);

        // when
        Iterable<ClientAccount> accounts = clientAccountRepository.findAllByClientAndAccountType(client, accountType);

        // then
        assertThat(((Collection<?>) accounts).size())
                .isGreaterThan(0);
    }

    @Test
    public void whenFindAllByClientAndAccountType_Transactional_thenReturnCorrectClientAccounts() {
        // Given
        Client client = new Client();
        client.setClientId(10);

        // And
        List<AccountType> accountTypes = new ArrayList<>();
        AccountType accountType0 = new AccountType();
        accountType0.setAccountTypeCode(AccountTypeCode.CHQ);

        AccountType accountType1 = new AccountType();
        accountType1.setAccountTypeCode(AccountTypeCode.SVGS);

        AccountType accountType2 = new AccountType();
        accountType2.setAccountTypeCode(AccountTypeCode.CCRD);

        accountTypes.add(accountType0);
        accountTypes.add(accountType1);
        accountTypes.add(accountType2);

        // when
        Iterable<ClientAccount> accounts = clientAccountRepository.findAllByClientAndAccountTypeIsIn(client, accountTypes);

        // then
        assertThat(((Collection<?>) accounts).size())
                .isNotZero();
    }

    @Test
    public void whenFindClientAccountByClientAndAccountNumber_thenReturnCorrectClientAccount() {
        // Given
        Client client = new Client();
        client.setClientId(10);
        //and
        String accountNumber = "4055230225";


        // when
        Optional<ClientAccount> accountOptional = clientAccountRepository.findClientAccountByClientAndAccountNumber(client, accountNumber);

        // then
        assertThat(accountOptional.isPresent())
                .isTrue();
    }
}