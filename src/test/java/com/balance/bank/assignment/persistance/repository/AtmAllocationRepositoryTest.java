package com.balance.bank.assignment.persistance.repository;

import com.balance.bank.assignment.persistance.entity.Atm;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class AtmAllocationRepositoryTest {

    @Autowired
    private AtmAllocationRepository atmAllocationRepository;

    @Test
    public void whenFindAllAtmAllocationsByAtm_thenReturnAllAllocation() {
        Atm atm = new Atm();
        atm.setAtmId(1);

        log.info("wwwoooooooooo=====================================      ===" + new BigDecimal(2).compareTo(new BigDecimal(3)) + "");
        assertThat(atmAllocationRepository.findAllAtmAllocationsByAtm(atm)
                .isPresent()).isTrue();
    }
}