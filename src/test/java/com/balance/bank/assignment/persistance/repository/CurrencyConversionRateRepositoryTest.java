package com.balance.bank.assignment.persistance.repository;

import com.balance.bank.assignment.persistance.entity.CurrencyConversionRate;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CurrencyConversionRateRepositoryTest {

    @Autowired
    private CurrencyConversionRateRepository currencyConversionRateRepository;

    @Test
    public void whenCountAll_thenReturnTotal() {
        assertThat(currencyConversionRateRepository.count())
                .isGreaterThan(-1);
    }

    @Test
    public void whenFindByAccountTypeCode_thenReturnAccountType() {
        // given
        String currencyCode = "USD";

        // when
        Optional<CurrencyConversionRate> found = currencyConversionRateRepository.findCurrencyConversionRateByCurrencyCode(currencyCode);

        // then
        assertThat(found.isPresent()).isTrue();
        //and
        assertThat(found.get().getRate()).isNotZero();

    }
}