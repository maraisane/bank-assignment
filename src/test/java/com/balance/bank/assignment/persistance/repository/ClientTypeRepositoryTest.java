package com.balance.bank.assignment.persistance.repository;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ClientTypeRepositoryTest {

    @Autowired
    private ClientTypeRepository clientTypeRepository;

    @Test
    public void whenCountAll_thenReturnTotal() {
        assertThat(clientTypeRepository.count())
                .isGreaterThan(0);
    }
}