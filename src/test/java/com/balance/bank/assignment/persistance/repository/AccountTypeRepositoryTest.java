package com.balance.bank.assignment.persistance.repository;

import com.balance.bank.assignment.persistance.entity.AccountType;
import com.balance.bank.assignment.persistance.enums.AccountTypeCode;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AccountTypeRepositoryTest {
    @Autowired
    private AccountTypeRepository accountTypeRepository;

    @Test
    public void whenCountAll_thenReturnTotal() {
        List<String> studentName = Arrays.asList("hector", "hana", "stone");

        List<String> result = studentName.stream()	// Convert list to stream
                .filter(name -> !"hana".equals(name))	// remove "hana" hana from list
                .collect(Collectors.toList());			// collect the output and convert Stream to List

// print using method reference
        result.forEach(System.out::println);

        final int totalAccountType = 6;

        Assert.assertEquals("", totalAccountType, accountTypeRepository.count());
    }

    @Test
    public void whenFindByAccountTypeCode_thenReturnAccountType() {
        // given
        AccountType accountType = new AccountType();
        accountType.setAccountTypeCode(AccountTypeCode.CHQ);
        accountType.setDescription("Cheque Account");
        accountType.setTransactional(true);
        accountTypeRepository.save(accountType);

        // when
        AccountType found = accountTypeRepository.findByAccountTypeCode(AccountTypeCode.CHQ);

        // then
        assertThat(found.getDescription())
                .isEqualTo(accountType.getDescription());
    }

    @Test
    public void whenFindAllByTransactionalIsTrue_thenReturnAllTransactionalAccountType() {

        // when
        Collection<AccountType> founds = accountTypeRepository.findAllByTransactionalIsTrue();

        // then
        assertThat(founds.size())
                .isGreaterThan(0);
        //and
//        assertThat(founds.forEach(x -> x.getTransactional())
//                .isGreaterThan(0);
    }
}