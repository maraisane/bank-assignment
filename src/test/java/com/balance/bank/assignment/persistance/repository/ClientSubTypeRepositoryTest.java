package com.balance.bank.assignment.persistance.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ClientSubTypeRepositoryTest {

    @Autowired
    private ClientSubTypeRepository clientSubTypeRepository;

    @Test
    public void whenCountAll_thenReturnTotal() {
        assertThat(clientSubTypeRepository.count())
                .isEqualTo(5);
    }
}

