# Bank Balance and Dispensing System

This application calculates and displays the financial position to a client on an ATM screen.  In addition, a client must 
also be able to mak a request for a cash withdrawal

## Running the application

These instructions will get you a copy of the project up and running on your local machine for development and testing 
purposes. Swagger open APIis used to test back-end Rest APIs

### Prerequisites

Before compiling this project, one has to ensure that following tools are installed 
 1. Java 8 
 2. Maven 3+
 3. The PC proxies are well set so that any dependency that is needed from the internet can be downloaded
 
 
## Question 4 Scripts
  
A db script for question 4 has can be found at; 
```/src/main/resources/question_4_2_4.sql
   /src/main/resources/question_4_2_5.sql
 ```
## Run the application as a spring boot
 1. Run maven commands 

```
mvn clean install
java -jar target/*.jar
```

The application uses provided schema.sql to create tables and populate those using 
data.sql provided.It uses  H2 DB. To browse the DB the following link can be used;
  
  ```$xslt
  http://localhost:8080/h2-console/login.jsp
  Driver Class :  org.h2.Driver
  JDBC URL: jdbc:h2:mem:testdb
  User Name : sa
  Password : sa
  ``` 
## Testing the Project with Swagger ui or curl command

   The application has three Rest API operations that can be used to test it.
   The Swagger URL has been configured to:
   
```
 http://localhost:8080/swagger-ui.html#/
```

 1. /api/transactional-accounts/withdrawCash/{client-id}
  
```
  Positive Test : curl -X POST "http://localhost:8080/api/transactional-accounts/withdraw/10" -H "accept: */*" -H "Content-Type: application/json" -d "{ \"accountNumber\": \"1002222785\", \"amount\": \"200\", \"atmId\": 3, \"clientTimeStamp\": \"2017-09-09\"}"
  
  Negative Test 'No accounts to display': curl -X POST "http://localhost:8080/api/transactional-accounts/withdraw/20" -H "accept: */*" -H "Content-Type: application/json" -d "{ \"accountNumber\": \"1002222785\", \"amount\": \"200\", \"atmId\": 3, \"clientTimeStamp\": \"2017-09-09\"}"
  
  Negative Test 'Insufficient funds': curl -X POST "http://localhost:8080/api/transactional-accounts/withdraw/10" -H "accept: */*" -H "Content-Type: application/json" -d "{ \"accountNumber\": \"1002222785\", \"amount\": \"200000\", \"atmId\": 3, \"clientTimeStamp\": \"2017-09-09\"}"
  
  Negative Test 'Amount not available, would you like to draw RX': curl -X POST "http://localhost:8080/api/transactional-accounts/withdraw/10" -H "accept: */*" -H "Content-Type: application/json" -d "{  \"accountNumber\": \"1002222785\", \"amount\": \"225\", \"atmId\": 3, \"clientTimeStamp\": \"2017-09-09\"}"
```
    
 2. /api/currency-accounts/{client-id}/{client-time-stamp}
 
```
Positive Test: curl -X GET "http://localhost:8080/api/currency-accounts/1/2007-09-09" -H "accept: */*"

Negative Test: curl -X GET "http://localhost:8080/api/currency-accounts/20/2007-09-09" -H "accept: */*"
```
 3. /api/transactional-accounts/{client-id}/{client-time-stamp}
 
```
Positive Test: curl -X GET "http://localhost:8080/api/transactional-accounts/10/2019-09-09" -H "accept: */*"
```
## Testing Using UI (for the scope of this project, I would like testing to be done on Swagger or curl command)
Even though, ATM screen is out of Scope for this project, Spring MVC has been used to give a display of ATM screens.

```
http://localhost:8080
```
1.Display transactional accounts with balances
```
http://localhost:8080/accounts/{client-id}
```
2.Display currency accounts with converted Rand values
```
http://localhost:8080/currency/{client-id}
```
3.Withdraw cash
```
http://localhost:8080/withdraw
```
    
## Assumptions
 1. The is no feedback needed to the ATM screen when the client withdrew coins, specification only determine but did 
 cater for the client  of that
 
 2. All the ATM are allocated local Currency Notes (ZAR), otherwise the implementation needs to know the allocated Currency and withdrawal conversion logic will be affected. 
 
## Out of scope or Proposed future development
 1. UI screen as per spec, they are now marked as out of scope. In future, the UI module will need to be separated from backend API.
 
 2. Enhance the scheduler so that it is configured to regularly email out the reports to the configured email address. 
 